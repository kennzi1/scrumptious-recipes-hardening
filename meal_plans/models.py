from django.conf import settings
from django.db import models


USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="user_meal_plans",
        on_delete=models.CASCADE,
        null=True
    )
    date = models.DateField()
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plan_recipes"
        )

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def __str__(self):
        return str(self.name)
